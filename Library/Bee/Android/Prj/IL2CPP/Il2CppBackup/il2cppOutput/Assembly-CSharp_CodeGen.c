﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void clock::Update()
extern void clock_Update_m7233996326C59C7AFABC09FB955DBE9FD8E07F7A (void);
// 0x00000002 System.Void clock::.ctor()
extern void clock__ctor_mB7FF97164224CFD5595BD68F4EAB09935C4B300C (void);
// 0x00000003 game_controller game_controller::get_Instance()
extern void game_controller_get_Instance_m127E8887F329B5DBA5091424057E7AD2D5B50BE9 (void);
// 0x00000004 System.Void game_controller::set_Instance(game_controller)
extern void game_controller_set_Instance_mC8D84003DE3404F449845D7E9359C1C245339003 (void);
// 0x00000005 System.Void game_controller::Awake()
extern void game_controller_Awake_m06418E93A2401B754CE752640F56E848474B09AA (void);
// 0x00000006 System.Void game_controller::Update()
extern void game_controller_Update_m290BE77FF7B8559ACACE53C8B435DE5D2977F0F9 (void);
// 0x00000007 System.Void game_controller::StartGame()
extern void game_controller_StartGame_mCD0420250B3BB72F6BFBCF2258DAFF88D047A410 (void);
// 0x00000008 System.Void game_controller::start_protect()
extern void game_controller_start_protect_m5F926D26587CF76A81B59B4DC810619F99B31276 (void);
// 0x00000009 System.Void game_controller::start_pound()
extern void game_controller_start_pound_m54E89CD4EA18DFFCB6B170F96869719BF83B06C6 (void);
// 0x0000000A System.Void game_controller::spawn_kitty()
extern void game_controller_spawn_kitty_m6D345BFFF28B22A7947DA1646F41BF646ADF20E2 (void);
// 0x0000000B System.Void game_controller::endGame()
extern void game_controller_endGame_m7628DE315F64131CDA6D6889A55F057C16C34097 (void);
// 0x0000000C System.Collections.IEnumerator game_controller::stop_playing()
extern void game_controller_stop_playing_mDC6A3A1685E6E04190F81CEBF3B7333D294C7D59 (void);
// 0x0000000D System.Void game_controller::.ctor()
extern void game_controller__ctor_m16E11A3276CC1E07D76669EB3BD9E1E252814B97 (void);
// 0x0000000E System.Void game_controller/<stop_playing>d__25::.ctor(System.Int32)
extern void U3Cstop_playingU3Ed__25__ctor_mF9271D0AD3C6A73EB0C51B868CA639940B1F03D5 (void);
// 0x0000000F System.Void game_controller/<stop_playing>d__25::System.IDisposable.Dispose()
extern void U3Cstop_playingU3Ed__25_System_IDisposable_Dispose_m5AB171D634A7C612FB1195026A83AB4039C49127 (void);
// 0x00000010 System.Boolean game_controller/<stop_playing>d__25::MoveNext()
extern void U3Cstop_playingU3Ed__25_MoveNext_m8DCF943427F731D8DDE96D29D5AEA72E009C478C (void);
// 0x00000011 System.Object game_controller/<stop_playing>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Cstop_playingU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11E7BC82AB429493ECEE5803E5DC5E638B0D6CD7 (void);
// 0x00000012 System.Void game_controller/<stop_playing>d__25::System.Collections.IEnumerator.Reset()
extern void U3Cstop_playingU3Ed__25_System_Collections_IEnumerator_Reset_m37A2BE6D0BFB0B71B75B2A7B2B5DCAF5AC0072D1 (void);
// 0x00000013 System.Object game_controller/<stop_playing>d__25::System.Collections.IEnumerator.get_Current()
extern void U3Cstop_playingU3Ed__25_System_Collections_IEnumerator_get_Current_m2771DA4690FF102251954B7052E4FD8C309C0316 (void);
// 0x00000014 System.Void kitty_controller::Start()
extern void kitty_controller_Start_mC4D751C87E4A3ED0A011D32121C675A84A55C3B7 (void);
// 0x00000015 System.Void kitty_controller::Update()
extern void kitty_controller_Update_mC3E959975932FD33B118951641C1549338308CA2 (void);
// 0x00000016 System.Void kitty_controller::reset_destination()
extern void kitty_controller_reset_destination_mD6B88FF9ABCC9EC35A16E700832CB208B30851E4 (void);
// 0x00000017 System.Void kitty_controller::pause_destination()
extern void kitty_controller_pause_destination_m3EE45E0758D9A0DAA818486FB5AFA5530EB74897 (void);
// 0x00000018 System.Void kitty_controller::unpause_destination()
extern void kitty_controller_unpause_destination_m2852FE39DFF99044687499BD6F536893355ABD24 (void);
// 0x00000019 System.Collections.IEnumerator kitty_controller::lick_wounds()
extern void kitty_controller_lick_wounds_mD49C2A3F2010346F1DB2006049E5514D22AB8E5D (void);
// 0x0000001A System.Void kitty_controller::OnCollisionEnter(UnityEngine.Collision)
extern void kitty_controller_OnCollisionEnter_mDB6D025020ECBA1FA471AF0C7A63F022E94A8C47 (void);
// 0x0000001B System.Void kitty_controller::meow()
extern void kitty_controller_meow_m5A1ED2D72879409955CFCFFC61EC3168CA33FE4C (void);
// 0x0000001C System.Void kitty_controller::reset_meow()
extern void kitty_controller_reset_meow_m58280232FCCF03E7A2B878BC47D6FC72AA6105A7 (void);
// 0x0000001D System.Void kitty_controller::.ctor()
extern void kitty_controller__ctor_m7CBC0275994A3AB44E0B05541BE61FA0CD8D2A00 (void);
// 0x0000001E System.Void kitty_controller/<lick_wounds>d__12::.ctor(System.Int32)
extern void U3Click_woundsU3Ed__12__ctor_mB430EA11C95A6AD627EF635AA766EF7FD3CA4BA8 (void);
// 0x0000001F System.Void kitty_controller/<lick_wounds>d__12::System.IDisposable.Dispose()
extern void U3Click_woundsU3Ed__12_System_IDisposable_Dispose_mDC135106B7C34D993A79E21C314A617F2C1AC15E (void);
// 0x00000020 System.Boolean kitty_controller/<lick_wounds>d__12::MoveNext()
extern void U3Click_woundsU3Ed__12_MoveNext_mD612699C03E328AD1FE23AD5A17548651D6E6BCE (void);
// 0x00000021 System.Object kitty_controller/<lick_wounds>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3Click_woundsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF32A2EB42F1B95A87FF4F1B6904412E7CE963D54 (void);
// 0x00000022 System.Void kitty_controller/<lick_wounds>d__12::System.Collections.IEnumerator.Reset()
extern void U3Click_woundsU3Ed__12_System_Collections_IEnumerator_Reset_mF16B6048A70647273B6EF5162F94CD5D4DBAB6A7 (void);
// 0x00000023 System.Object kitty_controller/<lick_wounds>d__12::System.Collections.IEnumerator.get_Current()
extern void U3Click_woundsU3Ed__12_System_Collections_IEnumerator_get_Current_m7CB3AC3D1BCD5F88AD85FF1F612512B4D1D22572 (void);
// 0x00000024 System.Void pounder::Start()
extern void pounder_Start_m2029DF998012A07B764D7459ED5C6B764D0A2482 (void);
// 0x00000025 System.Void pounder::Update()
extern void pounder_Update_mADE7317583943D338F58E83B7FC586F587601AAE (void);
// 0x00000026 System.Collections.IEnumerator pounder::dropHammer()
extern void pounder_dropHammer_mEE9AC9157AB3B1A9BA38FE9DD8E9932586029E15 (void);
// 0x00000027 System.Void pounder::.ctor()
extern void pounder__ctor_m0CE51703383EC048D4A57ACA71636948C18CC596 (void);
// 0x00000028 System.Void pounder/<dropHammer>d__9::.ctor(System.Int32)
extern void U3CdropHammerU3Ed__9__ctor_mB050073F0684548A0E5D45B8927C08642C8F5FD9 (void);
// 0x00000029 System.Void pounder/<dropHammer>d__9::System.IDisposable.Dispose()
extern void U3CdropHammerU3Ed__9_System_IDisposable_Dispose_m13F9C1FB97D5F5B078D0369E691CC7669F90B0F9 (void);
// 0x0000002A System.Boolean pounder/<dropHammer>d__9::MoveNext()
extern void U3CdropHammerU3Ed__9_MoveNext_m0882081A64878E854A0A6D40AB1052D56C283F1B (void);
// 0x0000002B System.Object pounder/<dropHammer>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdropHammerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49226A9C644E914C968EAA6B36AE17BC1B69F95B (void);
// 0x0000002C System.Void pounder/<dropHammer>d__9::System.Collections.IEnumerator.Reset()
extern void U3CdropHammerU3Ed__9_System_Collections_IEnumerator_Reset_m24107E3CC7AB11CC75A65EBBB83494806EDEDB33 (void);
// 0x0000002D System.Object pounder/<dropHammer>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CdropHammerU3Ed__9_System_Collections_IEnumerator_get_Current_mB73FE9D4F768F578BD18AA97037C0D9565CDA6CF (void);
// 0x0000002E System.Void squished::Start()
extern void squished_Start_mCC910EF8829EF5C62C8A1615F3952602DE46A713 (void);
// 0x0000002F System.Collections.IEnumerator squished::unstick()
extern void squished_unstick_mDB5F89F751CF244B7137EABC8B8D7C1499C4C76F (void);
// 0x00000030 System.Void squished::.ctor()
extern void squished__ctor_m469344BE569EF96BD95F0E7199D0E676495109BA (void);
// 0x00000031 System.Void squished/<unstick>d__2::.ctor(System.Int32)
extern void U3CunstickU3Ed__2__ctor_m7A22141171E515E559682CA0740D133A2CA0CC0E (void);
// 0x00000032 System.Void squished/<unstick>d__2::System.IDisposable.Dispose()
extern void U3CunstickU3Ed__2_System_IDisposable_Dispose_mD84E0C4902F9B68DBCE0FC990616A4BCAAB2A08B (void);
// 0x00000033 System.Boolean squished/<unstick>d__2::MoveNext()
extern void U3CunstickU3Ed__2_MoveNext_m5D10B2F5048807076E5BD5346709376F8A0CA32C (void);
// 0x00000034 System.Object squished/<unstick>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CunstickU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FCF5F76285F8EF0524864F9F31818E7DE48FBBC (void);
// 0x00000035 System.Void squished/<unstick>d__2::System.Collections.IEnumerator.Reset()
extern void U3CunstickU3Ed__2_System_Collections_IEnumerator_Reset_m00F5B3C49C9C05122D805FB07AAD4AD174654983 (void);
// 0x00000036 System.Object squished/<unstick>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CunstickU3Ed__2_System_Collections_IEnumerator_get_Current_m2D385AFBF04BCD894DF1EFB2E0F412D42490A4A7 (void);
// 0x00000037 System.Void Unity.Template.VR.XRPlatformControllerSetup::Start()
extern void XRPlatformControllerSetup_Start_m1F22FCA29DFD83DC0E343C3F391D04A7C52085BF (void);
// 0x00000038 System.Void Unity.Template.VR.XRPlatformControllerSetup::.ctor()
extern void XRPlatformControllerSetup__ctor_mF9A3998AF90962CF8F35BAF2221558BDF5F6596E (void);
static Il2CppMethodPointer s_methodPointers[56] = 
{
	clock_Update_m7233996326C59C7AFABC09FB955DBE9FD8E07F7A,
	clock__ctor_mB7FF97164224CFD5595BD68F4EAB09935C4B300C,
	game_controller_get_Instance_m127E8887F329B5DBA5091424057E7AD2D5B50BE9,
	game_controller_set_Instance_mC8D84003DE3404F449845D7E9359C1C245339003,
	game_controller_Awake_m06418E93A2401B754CE752640F56E848474B09AA,
	game_controller_Update_m290BE77FF7B8559ACACE53C8B435DE5D2977F0F9,
	game_controller_StartGame_mCD0420250B3BB72F6BFBCF2258DAFF88D047A410,
	game_controller_start_protect_m5F926D26587CF76A81B59B4DC810619F99B31276,
	game_controller_start_pound_m54E89CD4EA18DFFCB6B170F96869719BF83B06C6,
	game_controller_spawn_kitty_m6D345BFFF28B22A7947DA1646F41BF646ADF20E2,
	game_controller_endGame_m7628DE315F64131CDA6D6889A55F057C16C34097,
	game_controller_stop_playing_mDC6A3A1685E6E04190F81CEBF3B7333D294C7D59,
	game_controller__ctor_m16E11A3276CC1E07D76669EB3BD9E1E252814B97,
	U3Cstop_playingU3Ed__25__ctor_mF9271D0AD3C6A73EB0C51B868CA639940B1F03D5,
	U3Cstop_playingU3Ed__25_System_IDisposable_Dispose_m5AB171D634A7C612FB1195026A83AB4039C49127,
	U3Cstop_playingU3Ed__25_MoveNext_m8DCF943427F731D8DDE96D29D5AEA72E009C478C,
	U3Cstop_playingU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11E7BC82AB429493ECEE5803E5DC5E638B0D6CD7,
	U3Cstop_playingU3Ed__25_System_Collections_IEnumerator_Reset_m37A2BE6D0BFB0B71B75B2A7B2B5DCAF5AC0072D1,
	U3Cstop_playingU3Ed__25_System_Collections_IEnumerator_get_Current_m2771DA4690FF102251954B7052E4FD8C309C0316,
	kitty_controller_Start_mC4D751C87E4A3ED0A011D32121C675A84A55C3B7,
	kitty_controller_Update_mC3E959975932FD33B118951641C1549338308CA2,
	kitty_controller_reset_destination_mD6B88FF9ABCC9EC35A16E700832CB208B30851E4,
	kitty_controller_pause_destination_m3EE45E0758D9A0DAA818486FB5AFA5530EB74897,
	kitty_controller_unpause_destination_m2852FE39DFF99044687499BD6F536893355ABD24,
	kitty_controller_lick_wounds_mD49C2A3F2010346F1DB2006049E5514D22AB8E5D,
	kitty_controller_OnCollisionEnter_mDB6D025020ECBA1FA471AF0C7A63F022E94A8C47,
	kitty_controller_meow_m5A1ED2D72879409955CFCFFC61EC3168CA33FE4C,
	kitty_controller_reset_meow_m58280232FCCF03E7A2B878BC47D6FC72AA6105A7,
	kitty_controller__ctor_m7CBC0275994A3AB44E0B05541BE61FA0CD8D2A00,
	U3Click_woundsU3Ed__12__ctor_mB430EA11C95A6AD627EF635AA766EF7FD3CA4BA8,
	U3Click_woundsU3Ed__12_System_IDisposable_Dispose_mDC135106B7C34D993A79E21C314A617F2C1AC15E,
	U3Click_woundsU3Ed__12_MoveNext_mD612699C03E328AD1FE23AD5A17548651D6E6BCE,
	U3Click_woundsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF32A2EB42F1B95A87FF4F1B6904412E7CE963D54,
	U3Click_woundsU3Ed__12_System_Collections_IEnumerator_Reset_mF16B6048A70647273B6EF5162F94CD5D4DBAB6A7,
	U3Click_woundsU3Ed__12_System_Collections_IEnumerator_get_Current_m7CB3AC3D1BCD5F88AD85FF1F612512B4D1D22572,
	pounder_Start_m2029DF998012A07B764D7459ED5C6B764D0A2482,
	pounder_Update_mADE7317583943D338F58E83B7FC586F587601AAE,
	pounder_dropHammer_mEE9AC9157AB3B1A9BA38FE9DD8E9932586029E15,
	pounder__ctor_m0CE51703383EC048D4A57ACA71636948C18CC596,
	U3CdropHammerU3Ed__9__ctor_mB050073F0684548A0E5D45B8927C08642C8F5FD9,
	U3CdropHammerU3Ed__9_System_IDisposable_Dispose_m13F9C1FB97D5F5B078D0369E691CC7669F90B0F9,
	U3CdropHammerU3Ed__9_MoveNext_m0882081A64878E854A0A6D40AB1052D56C283F1B,
	U3CdropHammerU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49226A9C644E914C968EAA6B36AE17BC1B69F95B,
	U3CdropHammerU3Ed__9_System_Collections_IEnumerator_Reset_m24107E3CC7AB11CC75A65EBBB83494806EDEDB33,
	U3CdropHammerU3Ed__9_System_Collections_IEnumerator_get_Current_mB73FE9D4F768F578BD18AA97037C0D9565CDA6CF,
	squished_Start_mCC910EF8829EF5C62C8A1615F3952602DE46A713,
	squished_unstick_mDB5F89F751CF244B7137EABC8B8D7C1499C4C76F,
	squished__ctor_m469344BE569EF96BD95F0E7199D0E676495109BA,
	U3CunstickU3Ed__2__ctor_m7A22141171E515E559682CA0740D133A2CA0CC0E,
	U3CunstickU3Ed__2_System_IDisposable_Dispose_mD84E0C4902F9B68DBCE0FC990616A4BCAAB2A08B,
	U3CunstickU3Ed__2_MoveNext_m5D10B2F5048807076E5BD5346709376F8A0CA32C,
	U3CunstickU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FCF5F76285F8EF0524864F9F31818E7DE48FBBC,
	U3CunstickU3Ed__2_System_Collections_IEnumerator_Reset_m00F5B3C49C9C05122D805FB07AAD4AD174654983,
	U3CunstickU3Ed__2_System_Collections_IEnumerator_get_Current_m2D385AFBF04BCD894DF1EFB2E0F412D42490A4A7,
	XRPlatformControllerSetup_Start_m1F22FCA29DFD83DC0E343C3F391D04A7C52085BF,
	XRPlatformControllerSetup__ctor_mF9A3998AF90962CF8F35BAF2221558BDF5F6596E,
};
static const int32_t s_InvokerIndices[56] = 
{
	4812,
	4812,
	7344,
	7213,
	4812,
	4812,
	4812,
	4812,
	4812,
	4812,
	4812,
	4705,
	4812,
	3779,
	4812,
	4613,
	4705,
	4812,
	4705,
	4812,
	4812,
	4812,
	4812,
	4812,
	4705,
	3806,
	4812,
	4812,
	4812,
	3779,
	4812,
	4613,
	4705,
	4812,
	4705,
	4812,
	4812,
	4705,
	4812,
	3779,
	4812,
	4613,
	4705,
	4812,
	4705,
	4812,
	4705,
	4812,
	3779,
	4812,
	4613,
	4705,
	4812,
	4705,
	4812,
	4812,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	56,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
