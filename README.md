2022-10-01 - 2022-10-02
David Joel Stevenson
me@davidjoelstevenson.com

Created for Ludum Dare 51

I don't know why, but as soon as I heard the theme of "Every 10 seconds," I immediately pictured cats getting smashed by hammers.  Is something wrong with me?

Made for Oculus Quest because that's the platform I've been trying to learn - this is technically the first game I've ever "finished."  Sorry for the folks that don't have a headset!

# Notes

The "EckTechGames" packages is an asset I use for all unity projects - it simply saves the project every time you hit the play button.  How that's not a standard Unity feature I'll never know.

I used "standard" packages, like XR Integration Toolkit and the experimental Unity AI NavMesh thingy.  

Everything else was created between 10am on 2022-10-01 and 12pm on 2022-10-02, including audio (horrible sound effects recorded on my iphone, and maybe 2-3 hours recording the background music).  I also slept for almost 4 hours, so I'm going to say I did the whole thing in 24 hours.

In sticking with the theme, I should point out that the hammer drops every 10 seconds, a new kitty spawns every ten seconds, and each background music section is 10 seconds long, so that every time a new part is added it's coming in the same time as a new kitty and hammer pound.  I got...I don't know...artsy?  tired?  at 3am, so the tag at the end of the song is just over 10 seconds, so after the music loops it loses the excact 10-second tie in with the other events.  But, hey, for 4 minutes it's right on cue.