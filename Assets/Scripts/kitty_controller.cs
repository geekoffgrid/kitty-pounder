/**
2022-10-02
David Joel Stevenson
me@davidjoelstevenson.com
**/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class kitty_controller : MonoBehaviour {
	NavMeshAgent agent;
	Vector3 current_destination;
	bool is_active = true;
	public GameObject dead;

	AudioSource player;
	float meow_countdown;
	float path_time;
	
	void Start() {
		player = GetComponent<AudioSource>();
		agent = GetComponent<NavMeshAgent>();
		reset_destination();
	}

    void Update() {
		if (is_active) {
			agent.SetDestination(current_destination);
		}

		if (!player.isPlaying) {
			if (meow_countdown <= 0) {
				meow();
			} else {
				meow_countdown -= Time.deltaTime;
			}

			if (is_active && Time.deltaTime - path_time > 30f) {
				reset_destination(); //band aid for kitties flying out of view and never returning
			}
		}
    }

	public void reset_destination() {
		int random = Random.Range(0,game_controller.Instance.pounders.Count);
		pounder obj = game_controller.Instance.pounders[random].GetComponent<pounder>();
		if (game_controller.Instance.game_mode == 1) {
			current_destination = obj.target.transform.position;
		} else if (game_controller.Instance.game_mode == 2) {
			current_destination = obj.target_alt.transform.position;
		}
		path_time = Time.deltaTime;
	}

	public void pause_destination() {
		is_active = false;
		agent.enabled = false;
		if (player.isPlaying) {
			player.Stop(); //stop meow in order to play annoyed sound
		}
		int random = Random.Range(0,game_controller.Instance.annoyed.Count);
		player.clip = game_controller.Instance.annoyed[random];
		player.pitch = Random.Range(0.75f,1.25f);
		player.Play();
		reset_meow();
	}

	public void unpause_destination() {
		StartCoroutine(lick_wounds());
	}

	IEnumerator lick_wounds () {
		yield return new WaitForSeconds(2f);
		agent.enabled = true;
		reset_destination();
		is_active = true;
	}

	private void OnCollisionEnter(Collision collision)
    {
		Collider obj = collision.collider;
		if (obj.tag == "Mallet" && obj.transform.parent.parent.GetComponent<pounder>().is_swinging) {
			game_controller.Instance.dead_kitties++;
			GameObject dead_cat = Instantiate(dead,transform.position,Quaternion.identity) as GameObject;
			HingeJoint sticky = dead_cat.AddComponent<HingeJoint>();
        	sticky.connectedBody = collision.rigidbody;
			JointSpring hingeSpring = sticky.spring;
        	hingeSpring.spring = 10;
        	hingeSpring.damper = 3;
        	sticky.spring = hingeSpring;
        	sticky.useSpring = true;
			Destroy(gameObject);
		}
	}

	void meow() {
		if (!player.isPlaying) {
			int random = Random.Range(0,game_controller.Instance.meows.Count);
			player.clip = game_controller.Instance.meows[random];
			player.pitch = Random.Range(0.75f,1.25f);
			player.Play();			
		}
		reset_meow();
	}

	void reset_meow() {
		meow_countdown = Random.Range(1f,12f);
	}
}
