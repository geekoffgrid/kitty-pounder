/**
2022-10-02
David Joel Stevenson
me@davidjoelstevenson.com
**/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class game_controller : MonoBehaviour
{
	public float game_mode = 1;
	public float countdown = 10f;
	public bool is_playing = true;
	public List<GameObject> pounders = new List<GameObject>{};
	public GameObject kitty;
	float spawn_radius = 5f;
	float spawn_height = 3f;
	public int kitty_count = 0;
	public int dead_kitties = 0;
	public List<AudioClip> meows = new List<AudioClip>{};
	public List<AudioClip> annoyed = new List<AudioClip>{}; 
	public List<AudioClip> death = new List<AudioClip>{}; 
	AudioSource player;
	public GameObject start_button;

	//central singleton class for shooting projects
	public static game_controller Instance {
		get;
		private set;
	}

	void Awake () {
		if (Instance == null) {
			Instance = this;
		}

		player = GetComponent<AudioSource>();
	}

    // Update is called once per frame
    void Update()
    {
        if (is_playing) {
			if (game_mode == 1 && dead_kitties > 3) {
				endGame();
			}

			if (game_mode == 2 && kitty_count - dead_kitties > 3) {
				endGame();
			}

			if (countdown <= 0) {
				countdown = 10f;
				spawn_kitty();
			} else {
				countdown -= Time.deltaTime;
			}
		}
    }


	public void StartGame() {
		start_button.transform.GetChild(0).gameObject.SetActive(false);
		countdown = 10f;
		dead_kitties = 0;
		kitty_count = 0;
		is_playing = true;
		spawn_kitty();
		player.Play();
	}

	public void start_protect() {
		game_mode = 1;
		StartGame();
	}

	public void start_pound() {
		game_mode = 2;
		StartGame();
	}

	void spawn_kitty() {
		//spawn at random place on surrounding circle (maybe from roof above?)
		var angle = Random.Range(-1f,1f)*Mathf.PI*2;
		float x = Mathf.Cos(angle)*spawn_radius;
		float z = Mathf.Sin(angle)*spawn_radius;
		float y = spawn_height;

		GameObject new_kitty = Instantiate(kitty,new Vector3(x,y,z),Quaternion.identity) as GameObject;
		kitty_count++;
	}

	public void endGame() {
		player.Stop();
		start_button.transform.GetChild(0).gameObject.SetActive(true);
		GameObject[] kitties = GameObject.FindGameObjectsWithTag("Kitty");
		for(int i=0; i<kitties.Length;i++){
			Destroy(kitties[i]);
		}
		StartCoroutine(stop_playing());
	}

	//Add slight delay so that Counter UI gets updated
	IEnumerator stop_playing() {
		yield return new WaitForSeconds(0.3f);
		is_playing = false;
	}
}
