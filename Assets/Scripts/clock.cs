/**
2022-10-02
David Joel Stevenson
me@davidjoelstevenson.com
**/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class clock : MonoBehaviour
{
	public TMP_Text face;
	public TMP_Text count;
	public TMP_Text dead;

    // Update is called once per frame
    void Update()
    {
        if (game_controller.Instance.is_playing) {
			face.text = game_controller.Instance.countdown.ToString("00.0");
			count.text = game_controller.Instance.kitty_count.ToString();
			dead.text = game_controller.Instance.dead_kitties.ToString();
		}
    }
}
