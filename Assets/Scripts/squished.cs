/**
2022-10-02
David Joel Stevenson
me@davidjoelstevenson.com
**/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class squished : MonoBehaviour {
	AudioSource player;

    void Start() {
		player = GetComponent<AudioSource>();
		int random = Random.Range(0,game_controller.Instance.death.Count);
		player.clip = game_controller.Instance.death[random];
		player.pitch = Random.Range(0.75f,1.25f);
		player.Play();
		StartCoroutine(unstick());
    }

	IEnumerator unstick() {
		yield return new WaitForSeconds(2f);
		Destroy(GetComponent<HingeJoint>());
		Destroy(gameObject,3f);
	}
}
