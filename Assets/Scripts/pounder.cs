/**
2022-10-02
David Joel Stevenson
me@davidjoelstevenson.com
**/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pounder : MonoBehaviour
{
	public GameObject hammer;
	public GameObject target;
	public GameObject target_alt;
	bool is_active = false;
	public bool is_swinging = false;
	float speed = 20f;
	AudioSource player;

	void Start() {
		player = GetComponent<AudioSource>();
	}

    void Update() {
		if (!is_active && game_controller.Instance.is_playing && game_controller.Instance.countdown < 0.1f) {
			StartCoroutine(dropHammer());
		}
    }

	IEnumerator dropHammer() {
		is_active = true;//prevent double function call
		is_swinging = true;

		player.pitch = Random.Range(0.75f,1.25f);
		player.Play();

		while (hammer.transform.rotation.eulerAngles.x > 0f && hammer.transform.rotation.eulerAngles.x < 300f) {
			hammer.transform.Rotate(-100f*Time.deltaTime*speed,0,0);
			yield return null;
		}
		
		//hammer dropped, make sure rotation is exact and pause
		hammer.transform.rotation = Quaternion.Euler(0, hammer.transform.eulerAngles.y, hammer.transform.eulerAngles.z);
		is_swinging = false;
		yield return new WaitForSeconds(0.5f);

		//if I do 0 < x < 90, it keeps going to 180 - will troubleshoot later, probaly eulerAngles
		while (90f - hammer.transform.rotation.eulerAngles.x > 2f) {
			hammer.transform.Rotate(5f*Time.deltaTime*speed,0,0);//reset hammer at slower speed
			yield return null;
		}

		//make sure hammer reset rotation is exact
		hammer.transform.rotation = Quaternion.Euler(90f, hammer.transform.eulerAngles.y, hammer.transform.eulerAngles.z);
		is_active = false;
	}
}
